class AddDescToAirports < ActiveRecord::Migration[6.0]
  def change
    add_column :airports, :desc, :string
  end
end
