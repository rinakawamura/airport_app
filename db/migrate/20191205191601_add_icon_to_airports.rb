class AddIconToAirports < ActiveRecord::Migration[6.0]
  def change
    add_column :airports, :icon, :string
  end
end
