class AddCelsiusToAirport < ActiveRecord::Migration[6.0]
  def change
    add_column :airports, :celsius, :string
  end
end
