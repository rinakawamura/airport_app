class CreateUserAirports < ActiveRecord::Migration[6.0]
  def change
    create_table :user_airports do |t|
      t.integer :user_id
      t.integer :airport_id

      t.timestamps
    end
  end
end
