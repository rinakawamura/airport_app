Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'airports#index'
  resources :airports
  post 'airports/new', to: 'airports#create'
end
