require 'net/http'
require 'json'
require 'tzinfo'

class AirportsController < ApplicationController

    def index
        @airports = Airport.all
    end

    def show
    end

    def new
        @airport = Airport.new
    end
    
    def create
      respond_to do |format|
        response = Net::HTTP.get_response('api.weather.gov', '/stations/K' + airport_params[:code] + '/observations/latest')
        loc = response.header['location']
        if (response.code == "301")
          response = Net::HTTP.get_response(URI.parse(loc))
        end
        if (response.code=="200")
          # airport was found by weather.gov api
          # the first condition is that something was returned at all (code=="200")
          # the second condition is that the response must contain the word county because otherwise the airport was not found          
          parsed_response = JSON.parse(response.body)
          airport_url = parsed_response["properties"]["station"]
          airport_data = JSON.parse(Net::HTTP.get_response(URI.parse(airport_url)).body)
          airport_name = airport_data["properties"]["name"]
          timezone_name = airport_data["properties"]["timeZone"]
          weather_desc = parsed_response["properties"]["textDescription"]
          weather_celsius = parsed_response["properties"]["temperature"]["value"]
          weather_icon = parsed_response["properties"]["icon"]

          #TODO Use timezone name and gem 'tzinfo' to display time in local time
          # tz = TZInfo::Timezone.get(timezone_name)
          @airport = Airport.new(airport_params)

          @airport.name = airport_name
          @airport.timezone = timezone_name
          @airport.desc = weather_desc
          @airport.celsius = weather_celsius
          @airport.icon = weather_icon

          if @airport.save
            format.html { redirect_to airports_path, notice: 'Airport was successfully added.' }
            format.json { render :show, status: :created, location: @airport }
          else
            format.html { render :new }
            format.json { render json: @airport.errors, status: :unprocessable_entity }
          end
        else
          format.html { redirect_to airports_path, notice: 'Airport was not found.' }
          format.json { render json: @airport.errors, status: :unprocessable_entity }
        end
      end
    end
    
    def destroy
      set_airport
      @airport.destroy
      respond_to do |format|
        format.html { redirect_to airports_url, notice: 'Airport was successfully removed.' }
        format.json { head :no_content }
      end
    end

    def update
      set_airport
      response = Net::HTTP.get_response('api.weather.gov', '/stations/K' + @airport.code + '/observations/latest')
      loc = response.header['location']
      if (response.code == "301")
        response = Net::HTTP.get_response(URI.parse(loc))
      end
      if (response.code=="200")
        # airport was found by weather.gov api
        # the first condition is that something was returned at all (code=="200")
        # the second condition is that the response must contain the word county because otherwise the airport was not found          
        parsed_response = JSON.parse(response.body)
        weather_desc = parsed_response["properties"]["textDescription"]
        weather_celsius = parsed_response["properties"]["temperature"]["value"]
        weather_icon = parsed_response["properties"]["icon"]
      end
      @airport.update(desc: weather_desc, celsius: weather_celsius, icon: weather_icon)
      redirect_to airports_url
    end
    
    private
    # Use callbacks to share common setup or constraints between actions.
    def set_airport
      @airport = Airport.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def airport_params
      params.require(:airport).permit(:code)
    end


    
    
end