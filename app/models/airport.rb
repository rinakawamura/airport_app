class Airport < ApplicationRecord
    has_many :users, through: :user_airports
    has_many :user_airports

    # validates :code, presence: true
    validates_presence_of :code, :message => "can't be empty"
    validates_uniqueness_of :code, :case_sensitive => false
end
