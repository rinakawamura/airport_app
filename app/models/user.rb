class User < ApplicationRecord
    has_many :airports, through: :user_airports
    has_many :user_airports
end
